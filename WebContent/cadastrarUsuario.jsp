<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="dao.UsuarioDao"%>
    <%@ page import="modelo.Usuario"%>
    <%@ page import="java.util.List"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Cadastro de Usuário</title>
</head>
<body>
<div style="width:800px; border:1px solid #CCC; margin: 0 auto;">
	<div style="float:left; width:200px">
		<%@include file="menu.jsp" %>
	</div>
<%
	String perfil = (String) session.getAttribute("perfil");
%>
<%
	if(perfil.equals("administrador")){
%>

	<div style="width: 580px; float: left;margin-top: 30px; margin-left: 10px; border: 1px solid #CCC;height: 400px; border-radius: 6px;">
		<fieldset>
		<legend>Cadastro de novo usuário</legend>
		<form name="Acoes" action="Acoes" style="width: 223px;" action="cadastrarUsuario" method="post">
		<input type="hidden" name="acao" value="cadastrar">
		<label>Nome:</label>
		<input type="text" name="nome">
		<br/>
		<label>Login:</label>
		<input type="text" name="usuario">
		<br/>
		<label>Senha:</label>
		<input type="password" name="senha">
		<br/>
		<label>Perfil:</label>
		<select name="perfil">
			<option value="">Selecione...</option>
			<option value="administrador">Administrador</option>
			<option value="consultas">Consultas</option>
			<option value="manutencao">Manuteção</option>			
		</select>
		<br/>
		<input type="submit" value="Cadastrar" style="float:right; cursor:pointer;">
		</form>
		</fieldset>
	</div>
<%}else{ %>
<div style="width: 580px; float: left;margin-top: 30px; margin-left: 10px; border: 1px solid #CCC;height: 400px; border-radius: 6px;">
	<h2 style="font-size: 18px; text-align: center;">Usuário não tem previlégio suficientes para acessar essa página!!</h2>
</div>
	<%
}
	%>
</div>
</body>
</html>