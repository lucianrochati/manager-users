<style type="text/css">
	
		body {
			padding:0px;
			margin:0px;
		}
		#menu{
			margin-top: 30px;
		}
		#menu ul {
			padding:0px;
			margin:0px;
			float: left;
			width: 200px;
			list-style:none;
			font:80% Tahoma;
			border: 1px solid #DEDEDE;
		}

		#menu ul li { 
			border-bottom: 1px solid #413A3A;
			padding: 4px;
		 }
		 #menu ul li a{
		 	color:#413A3A;
			 text-decoration:none;
		 }
		 #menu ul li a:hover{
		 	color:#999;
			 text-decoration:none;
		 }

	</style>
	 <div id="menu"> 
		<ul>
			<li><a href="cadastrarUsuario.jsp">Cadastrar Usu�rio</a></li>
			<li><a href="redefinirDados.jsp">Redefinir dados do usu�rio</a></li>
			<li><a href="redefinirSenha.jsp">Redefinir senha para valor padr�o</a></li>
			<li><a href="mostrarQtdUsuarios.jsp">Mostrar quantidativos de usu�rios</a></li>
		</ul>
	 </div>
