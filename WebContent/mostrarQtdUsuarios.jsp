<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="dao.UsuarioDao" %>
<%@page import="dao.UsuarioDao" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Redefinição de Usuário</title>
</head>
<body>
<div style="width:800px; border:1px solid #CCC; margin: 0 auto;">
	<div style="float:left; width:200px">
		<%@include file="menu.jsp" %>
	</div>
<%
	UsuarioDao dao = new UsuarioDao();
	int totalUsuarios = dao.contarRegistros();
	int totalInativos = dao.quantidadeInativo();
%>
	<div style="width: 580px; float: left;margin-top: 30px; margin-left: 10px; border: 1px solid #CCC;height: 400px; border-radius: 6px;">
	<h2 style="font-size:14px;">Quantitativos de Usuários</h2>
	<br>
	<br>
	A quantidade total de usuários é:<%=totalUsuarios %><br><br>
	A quantidade de usuário que não acessam o sistema a mais de 2 dias é:<%=totalInativos %>
	</div>
	</div>
</body>
</html>