<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Redefinição de Usuário</title>
</head>
<body>
<div style="width:800px; border:1px solid #CCC; margin: 0 auto;">
	<div style="float:left; width:200px">
		<%@include file="menu.jsp" %>
	</div>
<%
	String id = (String) session.getAttribute("idUsuario");
	String nome = (String) session.getAttribute("nomeUsuario");
	String login = (String) session.getAttribute("login");
	String senha = (String) session.getAttribute("senha");
	String perfil = (String) session.getAttribute("perfil");
%>
	<div style="width: 580px; float: left;margin-top: 30px; margin-left: 10px; border: 1px solid #CCC;height: 400px; border-radius: 6px;">
		<fieldset>
		<legend>Cadastro de novo usuário</legend>

		<form name="Acoes" action="Acoes" style="width: 223px;" action="cadastrarUsuario" method="post">
		<input type="hidden" name="acao" value="redefinir_cadastro">
		<input type="hidden" name="id" value="<%=id%>">
		<label>Nome:</label>
		<input type="text" name="nome" value="<%=nome%>">
		<br/>
		<label>Login:</label>
		<input type="text" name="usuario" value="<%=login%>">
		<br/>
		<label>Senha:</label>
		<input type="password" name="senha" value="<%=senha%>">
		<br/>
		<label>Perfil:</label>
		<select name="perfil">
			<option value="">Selecione...</option>
			<option value="administrador" <%if(perfil.equals("administrador")){%>selected <%} %>>Administrador</option>
			<option value="consultas"<%if(perfil.equals("consultas")){%>selected <%} %>>Consultas</option>
			<option value="manutencao"<%if(perfil.equals("manutencao")){%>selected <%} %>>Manuteção</option>			
		</select>
		<br/>
		<input type="submit" value="Cadastrar" style="float:right; cursor:pointer;">
		</form>
		</fieldset>
	</div>
	</div>
</body>
</html>