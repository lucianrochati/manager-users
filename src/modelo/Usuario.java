package modelo;

public class Usuario {
	private String login;
	private String senha;
	private String nomeDoUsuario;
	private String perfil;
	private String dataAcesso;
	private String id;
	private int total;
	
	public void setTotal(int total){
		this.total = total;
	}
	
	public int getTotal(){
		return this.total;
	}
	
	public String getId(){
		return id;
	}
	
	public void setId(String id){
		this.id = id;
	}
	
	public String getNomeDoUsuario() {
		return nomeDoUsuario;
	}
	
	public void setNomeDoUsuario(String nomeDoUsuario) {
		this.nomeDoUsuario = nomeDoUsuario;
	}
	
	public String getPerfil() {
		return perfil;
	}
	
	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}
	
	public String getDataAcesso() {
		return dataAcesso;
	}
	
	public void setDataAcesso(String dataAcesso) {
		this.dataAcesso = dataAcesso;
	}
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	public boolean validaLogin() {
		boolean resp;
		if (login.length() < 3 )
			resp = false;
		else resp = true;
		return resp;
	}
	

}
