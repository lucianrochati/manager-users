package servlets;

import java.io.IOException;
import java.io.*;  

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelo.Usuario;
import dao.UsuarioDao;

/**
 * Servlet implementation class Acoes
 */
@WebServlet(value="/Acoes")

public class Acoes extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		Usuario user = new Usuario();
		UsuarioDao dao = new UsuarioDao();
		boolean retorno = false;
		PrintWriter saida =  response.getWriter();
		
		
		String acao = request.getParameter("acao");
		if(request.getParameter("id") != "")
				user.setId(request.getParameter("id"));
		if(request.getParameter("nome") != "")
			user.setNomeDoUsuario(request.getParameter("nome"));
		if(request.getParameter("usuario") != "")
			user.setLogin(request.getParameter("usuario"));
		if(request.getParameter("senha") != "")
			user.setSenha(request.getParameter("senha"));
		if(request.getParameter("perfil") != "")
			user.setPerfil(request.getParameter("perfil"));

		String nova_senha = request.getParameter("nova_senha");	

		if(acao.equals("cadastrar")){
			dao.inserir(user);
			retorno = true;
			saida.println("<html>"
					+ "<header>"
					+ "<script>alert('Usuário cadastrado com sucesso!!!');window.location='cadastrarUsuario.jsp';</script>"
					+ "</header>"
					+ "</html>");
			saida.close();
		}else if(acao.equals("redefinir_cadastro")){
			dao.atualizarUsuario(user);
			saida.println("<html>"
					+ "<header>"
					+ "<script>alert('Dados atualizados com sucesso!! faça login novamente!');window.location='index.jsp';</script>"
					+ "</header>"
					+ "</html>");
			saida.close();
		}else if(acao.equals("redefinir_senha")){
			dao.redefinirSenha(user);
			saida.println("<html>"
					+ "<header>"
					+ "<script>alert('Senha redefinida com sucesso!!');window.location='redefinirSenha.jsp';</script>"
					+ "</header>"
					+ "</html>");
			saida.close();
			
		}else if(acao.equals("alterar_senha")){
			dao.alterarSenha(nova_senha,user);
			saida.println("<html>"
					+ "<header>"
					+ "<script>alert('Senha alterada com sucesso!!!Faça login novamente!');window.location='index.jsp';</script>"
					+ "</header>"
					+ "</html>");
			saida.close();
		}
		
	}

}
