package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.*;
import modelo.*;

@WebServlet(value="/autenticar")
public class Validacao extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		response.setContentType("text/html");
		PrintWriter saida =  response.getWriter();
		
		Date data_acesso = new Date(System.currentTimeMillis());
		
		
		String login = request.getParameter("usuario");
		String senha = request.getParameter("senha");
		
		UsuarioDao dao = new UsuarioDao();
		List<Usuario> lista = new UsuarioDao().getLista();
		boolean retorno = false;
		
		for (Usuario u: lista) {

			if (login.equals(u.getLogin()) && senha.equals(u.getSenha())) { 
				retorno = true;
			
				if(senha.equals("123456")){
					saida.println("<html>"
							+ "<header>"
							+ "<script>alert('Sua senha é senha padrão, e precisa ser alterada, voce esta sendo redirecionado');window.location='alterarSenha.jsp';</script>"
							+ "</header>"
							+ "</html>");
							saida.close();
				}else{
				request.getSession().setAttribute("idUsuario", u.getId());
				request.getSession().setAttribute("nomeUsuario", u.getNomeDoUsuario());
				request.getSession().setAttribute("login", u.getLogin());
				request.getSession().setAttribute("senha", u.getSenha());
				request.getSession().setAttribute("perfil", u.getPerfil());
				request.getSession().setAttribute("data_acesso", data_acesso);
				request.getSession().setAttribute("contUsuario", 0);
				/*Roda triger para atualizar data de acesso no sistema */
					dao.atualizarHoraAcesso(data_acesso,u.getId());
				}
			}
		}
			

		if (retorno) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("gerenciador.jsp");
			dispatcher.forward(request, response);
		}
		else response.sendRedirect("index.jsp");
		
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		doPost(request, response);
	}
	
}
